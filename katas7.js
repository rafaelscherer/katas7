// let notas = [10,9,8,7,9,10,7,8];

// function filtraAprovados(item) {
//     let maior = false;
//     if (item > 7) {
//         maior = true;
//     }
//     return maior;
// }

// function multiplica2(item) {
//     let result = item * 2;
//     console.log (result);
// }

//////////
//katas1//
//////////
function newForEach(array, callback) {
    
    for (let i = 0; i < array.length; i++){
        let currentValue = array[i];
        callback(currentValue, i, array);        
    }    
}

//////////
//katas2//
//////////
function newMap(array, callback) {
    let output = [];

    for (let i = 0; i < array.length; i++){
        let item = array[i];
        let result = callback(item, i, array);
        output.push(result);
    }
    return output;
}    


//////////
//katas3//
//////////
function newSome(array, callback){    

    for (let i = 0; i < array.length; i++){
        let item = array[i];
        let result = callback(item, i, array);

        if (result === true){
            return true;            
        }
        return false; 
    }       
}

//////////
//katas4//
//////////
function newFind(array, callback){
    
    for (let i = 0; i < array.length; i++){
        let item = array[i];
        let result = callback(item, i, array);

        if (result === true){
            return item;          
        }
    }         
}

//////////
//katas5//
//////////
function newFindIndex(array, callback){

    for (let i = 0; i < array.length; i++){
        let item = array[i];
        let result = callback(item, i, array);

        if (result === true){
            return i;          
        } else
            return -1;
    } 
}

//////////
//katas6//
//////////
function newEvery(array, callback){

    for (let i = 0; i < array.length; i++){
        let item = array[i];
        let result = callback(item, i, array);

        if (result !== true){
            return false;
        } 
    }
    return true;
}

//////////
//katas7//
//////////
function newFilter(array, callback){
    let output = [];

    for (let i = 0; i < array.length; i++){
        let item = array[i];
        let result = callback(item, i, array);

        if (result === true){
            output.push(item);
        }
    }
    return output;
}